import { useState } from "react"
import { useLoadedData } from "./useLoadedData"
import { MultiSelect } from "./MultiSelect"
import { transformData } from "./transformData"
import { Chart } from "./Chart"
import { makeStyles, createStyles } from "@material-ui/core/styles"

import {
  Grid,
  Box,
  AppBar,
  Toolbar,
  Typography,
  CircularProgress,
} from "@material-ui/core"

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      flexGrow: 1,
    },
  })
)

function App() {
  const { loading, loadedData, errorMsg } = useLoadedData()

  const [campaigns, setCampaigns] = useState<string[]>([])
  const [sources, setSources] = useState<string[]>([])

  const transformedData = transformData(loadedData?.data || [], {
    campaigns,
    sources,
  })
  const classes = useStyles()

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6">ETL</Typography>
        </Toolbar>
      </AppBar>

      <Grid container>
        <Grid item md={3}>
          <Box p={2}>
            <MultiSelect
              id="campaigns"
              label="Campaigns"
              value={campaigns}
              onChange={setCampaigns}
              values={loadedData?.campaigns || []}
            />
            <MultiSelect
              id="sources"
              label="Sources"
              value={sources}
              onChange={setSources}
              values={loadedData?.sources || []}
            />
          </Box>
        </Grid>
        <Grid item md={9}>
          {loading ? (
            <CircularProgress />
          ) : errorMsg ? (
            errorMsg
          ) : (
            <Chart data={transformedData} />
          )}
        </Grid>
      </Grid>
    </div>
  )
}

export default App
