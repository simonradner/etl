import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts"
import { AdTransformedData } from "./types"

interface Props {
  data: AdTransformedData[]
}

// For simplicity, basically copied from https://codesandbox.io/s/simple-line-chart-kec3v?file=/src/styles.css
export function Chart({ data }: Props) {
  return (
    <LineChart
      width={800}
      height={600}
      data={data}
      margin={{
        top: 5,
        right: 30,
        left: 20,
        bottom: 5,
      }}
    >
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis
        scale="time"
        dataKey="timestamp"
        tickFormatter={(timestamp) => new Date(timestamp).toLocaleDateString()}
      />
      <YAxis scale="sqrt" />
      <Tooltip
        labelFormatter={(value) => new Date(value).toLocaleDateString()}
      />
      <Legend verticalAlign="top" height={36} />
      <Line type="basis" dataKey="clicks" stroke="blue" dot={false} />
      <Line type="basis" dataKey="impressions" stroke="black" dot={false} />
    </LineChart>
  )
}
