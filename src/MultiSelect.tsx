import Chip from "@material-ui/core/Chip"
import TextField from "@material-ui/core/TextField"
import Autocomplete from "@material-ui/lab/Autocomplete"

// Basically copied from https://material-ui.com/components/autocomplete/#multiple-values
// for simplicity
/* eslint-disable no-use-before-define */

interface Props {
  values: string[]
  value: string[] // The selected values
  onChange: (newV: string[]) => void
  label: string
  id: string
}

export function MultiSelect({ values, value, onChange, label, id }: Props) {
  return (
    <Autocomplete
      fullWidth
      multiple
      id={id}
      value={value}
      onChange={(event, newValue) => {
        onChange(newValue)
      }}
      style={{ paddingTop: "10px" }}
      options={values}
      getOptionLabel={(option) => option}
      renderTags={(tagValue, getTagProps) =>
        tagValue.map((option, index) => (
          <Chip label={option} {...getTagProps({ index })} />
        ))
      }
      renderInput={(params) => (
        <TextField {...params} label={label} variant="outlined" />
      )}
    />
  )
}
