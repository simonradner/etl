import { AdLoadedData, LoadedData } from "./types"

export function parseData(rawData: string): LoadedData | undefined {
  const { data, sources, campaigns } = rawData.split("\n").reduce<{
    data: AdLoadedData[]
    campaigns: Set<string>
    sources: Set<string>
  }>(
    (acc, c, i) => {
      if (i === 0) {
        return acc // First line is the header, we skip it.
      }
      const [datestring, source, campaign, clicks, impressions] = c.split(",")
      if (source && campaign) {
        // Ignore empty campaigns or sources.
        const timestamp = europeanDateToTimestamp(datestring) // Convert date to integer (timestamp) for easier usage in the chart

        // Ignore invalid dates.
        if (!isNaN(timestamp)) {
          acc.data.push({
            timestamp,
            source,
            campaign,
            clicks: Number(clicks),
            impressions: Number(impressions),
          })
          acc.sources.add(source)
          acc.campaigns.add(campaign)
        }
      }

      return acc
    },
    { data: [], campaigns: new Set(), sources: new Set() }
  )
  return {
    data,
    sources: Array.from(sources),
    campaigns: Array.from(campaigns),
  }
}

/**
 *  Transforms european dateformat '15.01.2019' to a timestamp.
 */
function europeanDateToTimestamp(date: string): number {
  const transformed = date.split(".").reverse().join("-")
  return Date.parse(transformed)
}
