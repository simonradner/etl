import { AdLoadedData, AdTransformedData } from "./types"
import { transformData } from "./transformData"

it("transforms empty data", () => {
  const input: AdLoadedData[] = []
  const output: AdTransformedData[] = []
  expect(transformData(input, { campaigns: [], sources: [] })).toEqual(output)
  expect(
    transformData(input, { campaigns: ["Foo"], sources: ["Bar"] })
  ).toEqual(output)
})

it("aggregates data", () => {
  const input: AdLoadedData[] = [
    { timestamp: 0, campaign: "A", source: "X", clicks: 1, impressions: 10 },
    { timestamp: 0, campaign: "B", source: "Y", clicks: 2, impressions: 20 },
    { timestamp: 0, campaign: "C", source: "Z", clicks: 3, impressions: 30 },
  ]
  const output: AdTransformedData[] = [
    {
      timestamp: 0,
      clicks: 6,
      impressions: 60,
    },
  ]
  expect(transformData(input, { campaigns: [], sources: [] })).toEqual(output)
})

it("aggregates data per timestamp", () => {
  const input: AdLoadedData[] = [
    { timestamp: 0, campaign: "A", source: "X", clicks: 1, impressions: 10 },
    { timestamp: 0, campaign: "B", source: "Y", clicks: 2, impressions: 20 },
    { timestamp: 1000, campaign: "C", source: "Z", clicks: 4, impressions: 40 },
  ]
  const output: AdTransformedData[] = [
    {
      timestamp: 0,
      clicks: 3,
      impressions: 30,
    },
    {
      timestamp: 1000,
      clicks: 4,
      impressions: 40,
    },
  ]
  expect(transformData(input, { campaigns: [], sources: [] })).toEqual(output)
})

it("respects filter for campaigns and sources", () => {
  const input: AdLoadedData[] = [
    { timestamp: 0, campaign: "A", source: "X", clicks: 1, impressions: 10 },
    { timestamp: 0, campaign: "B", source: "Y", clicks: 2, impressions: 20 },
    { timestamp: 0, campaign: "C", source: "Z", clicks: 3, impressions: 30 },
    { timestamp: 1000, campaign: "A", source: "X", clicks: 4, impressions: 40 },
    { timestamp: 1000, campaign: "B", source: "Y", clicks: 5, impressions: 50 },
    { timestamp: 1000, campaign: "C", source: "Z", clicks: 6, impressions: 60 },
  ]
  expect(transformData(input, { campaigns: [], sources: ["X"] })).toEqual([
    {
      timestamp: 0,
      clicks: 1,
      impressions: 10,
    },
    {
      timestamp: 1000,
      clicks: 4,
      impressions: 40,
    },
  ])
  expect(transformData(input, { campaigns: [], sources: ["Y", "Z"] })).toEqual([
    {
      timestamp: 0,
      clicks: 5,
      impressions: 50,
    },
    {
      timestamp: 1000,
      clicks: 11,
      impressions: 110,
    },
  ])
  expect(
    transformData(input, { campaigns: ["B"], sources: ["Y", "Z"] })
  ).toEqual([
    {
      timestamp: 0,
      clicks: 2,
      impressions: 20,
    },
    {
      timestamp: 1000,
      clicks: 5,
      impressions: 50,
    },
  ])
  expect(
    transformData(input, {
      campaigns: ["A", "B", "C"],
      sources: ["X", "Y", "Z"],
    })
  ).toEqual([
    {
      timestamp: 0,
      clicks: 6,
      impressions: 60,
    },
    {
      timestamp: 1000,
      clicks: 15,
      impressions: 150,
    },
  ])
  expect(
    transformData(input, { campaigns: ["NOT_EXIST"], sources: ["Y", "Z"] })
  ).toEqual([])
})
