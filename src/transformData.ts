import { AdLoadedData, AdTransformedData, Timestamp, Aggregate } from "./types"

/**
 * Transform data to a readable form for the chart.
 */
export function transformData(
  data: AdLoadedData[],
  { campaigns, sources }: { campaigns: string[]; sources: string[] }
): AdTransformedData[] {
  const aggregator = data.reduce((acc, c) => {
    // Filter out unselected campaigns.
    if (campaigns.length && !campaigns.includes(c.campaign)) {
      return acc
    }
    // Filter out unselected sources.
    if (sources.length && !sources.includes(c.source)) {
      return acc
    }

    // We're through, the data should be included,
    // make addition of new value to previous value.
    acc.aggregate(c.timestamp, (previous) => ({
      clicks: previous.clicks + c.clicks,
      impressions: previous.impressions + c.impressions,
    }))

    return acc
  }, createAggregator())

  return aggregator.transform()
}

interface AggregatedMetrics {
  clicks: Aggregate
  impressions: Aggregate
}
// Convenience helper to aggregate data for clicks and impressions per timestamp.
type AggregationFn = (
  timestamp: Timestamp,
  update: (previous: AggregatedMetrics) => AggregatedMetrics
) => void
interface AdAggregator {
  transform: () => AdTransformedData[]
  aggregate: AggregationFn
}

function createAggregator(): AdAggregator {
  const map = new Map<Timestamp, AggregatedMetrics>()

  const aggregate: AggregationFn = (timestamp, update) => {
    const previousValues = map.get(timestamp) || { clicks: 0, impressions: 0 }
    map.set(timestamp, update(previousValues)) // @TODO: Evaluate to use BigInt here, as aggregatied number could be huge.
  }

  return {
    transform: () =>
      Array.from(map)
        .sort(
          ([aTimestamp], [bTimestamp]) => (aTimestamp < bTimestamp ? -1 : 1) // Sort in decending timestamp order (Seems like Set sorts it anyway).
        )
        .map(([timestamp, metrics]) => ({ timestamp, ...metrics })),
    aggregate,
  }
}
