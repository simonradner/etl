export type Timestamp = number
export type Aggregate = number

export interface AdLoadedData {
  timestamp: Timestamp
  source: string
  campaign: string
  clicks: Aggregate
  impressions: Aggregate
}

export interface LoadedData {
  data: AdLoadedData[]
  sources: string[]
  campaigns: string[]
}

export interface AdTransformedData {
  timestamp: Timestamp
  clicks: Aggregate
  impressions: Aggregate
}
