import { LoadedData } from "./types"
import { useEffect, useState } from "react"
import { parseData } from "./parseData"

// @TODO: Src is not served via HTTPS, request a secure source.
const DATA_SRC =
  "http://adverity-challenge.s3-website-eu-west-1.amazonaws.com/DAMKBAoDBwoDBAkOBAYFCw.csv"

interface LoadedDataApi {
  loading: boolean
  errorMsg?: string
  loadedData?: LoadedData
}
export function useLoadedData(): LoadedDataApi {
  const [errorMsg, setErrorMsg] = useState<string | undefined>()
  const [loading, setLoading] = useState(false)
  const [data, setData] = useState<LoadedData | undefined>()

  useEffect(() => {
    async function fetchData() {
      setErrorMsg(undefined)
      setLoading(true)
      try {
        const res = await fetch(DATA_SRC)
        if (res.ok) {
          const rawData = await res.text()

          const data = parseData(rawData) // Undefined means we couldn't parse the src.
          setData(data) // @TODO: Handle undefined case instead of ignoring it.
        } else {
          setErrorMsg(res.toString())
        }
      } catch (error: unknown) {
        if (error) {
          console.error(error)
          if (typeof error === "string") {
            setErrorMsg(error)
          } else if (typeof error === "object" && "toString" in error!) {
            setErrorMsg(error.toString())
          } else {
            setErrorMsg("Unknown Error")
          }
        }
      }
      setLoading(false)
    }
    fetchData()
  }, [])

  return {
    loading,
    errorMsg,
    loadedData: data,
  }
}
